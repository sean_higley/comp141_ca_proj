// Sean and Patrick COMP141 CA6
#include <iostream>
#include <string>
#include <regex>
using namespace std;

int main() {

   string input;
   regex punct ("[*]|[(]|[+]|[)]");
   regex number ("^[0-9]+");
   cout<<"enter: -1 to exit"<<endl;
   getline(cin, input);
   while(input != "-1"){
      cout<<"enter: -1 to exit"<<endl;
      if(regex_match(input,punct)){
         cout<<"punctuation"<<endl;
      }
      else if(regex_match(input,number)){
         cout<<"number"<<endl;
      }
      else{
         cout<<"neither number nor punctuation"<<endl;
      }
      getline(cin, input);
   }

   return 0;
}